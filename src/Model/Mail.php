<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class sendMail{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public static function trySendMail($assunto, $destinatario, $html, $mail){

        if(empty($assunto) || empty($destinatario) || empty($html)){ return false; }

        //$mail = new PHPMailer(true);
        try {
          $mail->isSMTP();
          $mail->SMTPOptions = array(
              'ssl' => array(
                  'verify_peer' => false,
                  'verify_peer_name' => false,
                  'allow_self_signed' => true
              )
          );
          $mail->Host       = 'hosting81.serverhs.org';
          $mail->SMTPAuth   = true;
          $mail->Username   = 'mail@unifycloud.pt';
          $mail->Password   = 'nwF-0NoJ^-.{';
          $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
          $mail->Port       = 25;

          $mail->setFrom('mail@unifycloud.pt', "=?utf-8?B?".base64_encode('Eurotenologia')."?=");
          $mail->addAddress($destinatario);
          //$mail->addAddress('renanpantojavilas@gmail.com');
          //$mail->addAddress('pedidos@gmail.com');

          $mail->isHTML(true);
          $mail->WordWrap = 70;
          $mail->charSet = "UTF-8";
          $mail->XMailer  = 'Eurotenologia';
          $mail->setLanguage('pt');
          $mail->Subject = "=?utf-8?B?".base64_encode($assunto)."?=";
          $mail->Body    = $html;

          $mail->send();
          return true;

      } catch (Exception $e) {
          return false;
      }
    }

    //CRUD

    public function insertCat($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO produtos_cat (nome) VALUES (:nome);');
            $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readCat($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCat($dados){
        $deletef = $this->mysql->prepare('UPDATE produtos_cat SET nome = :nome WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM produtos_cat WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}




?>
