<?php

$enderecos = new Enderecos(new Config());
$mensagem_erro = false;

if($action == 'listar'){

  $loader->get('src/Model/Lojas');
  $lojas = new Lojas(new Config());
  $lojas_lista = array_column($lojas->readLojas(), NULL, 'id');
  $enderecos_lista = $enderecos->readEnderecos();

  // echo '<pre>';
  // print_r($lojas_lista);
  // echo '</pre>';

} else if($action == 'inserir'){

  $loader->get('src/Model/Lojas');
  $lojas = new Lojas(new Config());
  $lojas_lista = $lojas->readLojas();

  if($_POST){
    if(!empty($_POST['codigo_postal']) && !empty($_POST['id_loja'])){

      $dados['nome_rua'] = is_null($_POST['nome_rua']) ? NULL : $_POST['nome_rua'] ;
      $dados['id_loja'] = $_POST['id_loja'];
      $dados['codigo_postal'] = empty($_POST['codigo_postal']) ? '0' : preg_replace("/[^0-9]/", "", $_POST['codigo_postal']) ;
      $dados['taxa_entrega'] = is_null($_POST['taxa_entrega']) || empty($_POST['taxa_entrega'])  ? NULL : str_replace(",",".",$_POST['taxa_entrega']) ;

      $result = $enderecos->insertEndereco($dados);
      if(substr($result, 0, 4) == 'erro'){
        switch ($result) {
            case 'erro':
              $mensagem_erro = '<strong>Erro!</strong> Esse código postal já está cadastrado no sistema!';
              break;
        }
      } else {
        header('Location: '.URL_BASE.'/enderecos/listar');
      }

    } else {
      $mensagem_erro = '<strong>Erro!</strong> Algum dado obrigatório ficou em falta!';
    }
  }

} else if($action == 'deletar'){

  $categoria = $cat->readCat($param);

  if($_SERVER['REQUEST_METHOD']=='POST'){

    $cat->deleteCat($param);
    header('Location: ../listar');

  }

} else if($action == 'editar'){

  if(empty($param)){ header('Location: '.URL_BASE.'/404'); }

  $loader->get('src/Model/Lojas');
  $lojas = new Lojas(new Config());
  $lojas_lista = $lojas->readLojas();
  $endereco = $enderecos->readEnderecos($param);

  if($_POST){
    if(!empty($_POST['codigo_postal']) && !empty($_POST['id_loja'])){

      $dados['nome_rua'] = is_null($_POST['nome_rua']) ? NULL : $_POST['nome_rua'] ;
      $dados['localidade'] = is_null($_POST['localidade']) ? NULL : $_POST['localidade'] ;
      $dados['id_loja'] = $_POST['id_loja'];
      $dados['codigo_postal'] = empty($_POST['codigo_postal']) ? '0' : preg_replace("/[^0-9]/", "", $_POST['codigo_postal']) ;
      $dados['taxa_entrega'] = is_null($_POST['taxa_entrega']) || empty($_POST['taxa_entrega'])  ? NULL : str_replace(",",".",$_POST['taxa_entrega']) ;
      $dados['ativo'] = $_POST['ativo'] == 'on' ? 1 : 0 ;

      // echo '<pre>';
      // print_r($dados);
      // echo '</pre>';exit();

      $result = $enderecos->editEndereco($dados, $endereco['id']);
      if(substr($result, 0, 4) == 'erro'){
        switch ($result) {
            case 'erro':
              $mensagem_erro = '<strong>Erro!</strong> Esse código postal já está cadastrado em outro registro!';
              break;
        }
      } else {
        header('Location: '.URL_BASE.'/enderecos/pesquisa');
      }

    } else {
      $mensagem_erro = '<strong>Erro!</strong> Algum dado obrigatório ficou em falta!';
    }
  }

} else if($action == 'pesquisa'){

  if(!empty($_POST['codigo'])){

    $endereco = $enderecos->buscarCP($_POST['codigo']);

    if(empty($endereco)){
      $mensagem_erro = '<strong>Erro!</strong> Não existe nenhum código como esse cadastrado no sistema!';
    } else { header('Location: '.URL_BASE.'/enderecos/editar/'.$endereco['id']); }

  }

}

// echo '<pre>';
// print_r($_POST);
// echo '</pre>';

?>
