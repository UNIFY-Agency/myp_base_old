<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$pedidos = new Pedidos(new Config());
$loader->get('src/Model/Lojas');
$lojas = new Lojas(new Config());
$loader->get('src/Model/Mail');

$mensagem_erro = false;

  if (Request::isAjax()){

    if($action == 'order_automatic_advance_toggle'){
      if($_POST['loja']){
        $automatic_advance = $lojas->getLojaAutomaticAdvance($_POST['loja']);
        if(empty($automatic_advance)){
          $lojas->createLojaAutomaticAdvance($_POST['loja']);
          echo json_encode(array('1', 'Avanço automático de pedidos ligado com sucesso!'));
          exit();
        } else if($automatic_advance['ligado']) {
          $lojas->LojaAutomaticAdvanceOff($_POST['loja']);
          echo json_encode(array('1', 'Avanço automático de pedidos desligado com sucesso!'));
          exit();
        } else {
          $lojas->LojaAutomaticAdvanceOn($_POST['loja']);
          echo json_encode(array('1', 'Avanço automático de pedidos ligado com sucesso!'));
          exit();
        }
      } else {
        echo json_encode(array('0', '<strong>Erro!</strong> Dados em falta'));
        exit();
      }
    }

    if($action == 'last_order_id'){
      echo $pedidos->lastOrderId($_POST['loja'])['id'];
      exit();
    }

    if($action == 'detalhe'){
      if($_POST['order_id']){
        $pedido_detalhes = $pedidos->getOrdersDetails($_POST['order_id']);
        $pedido_cupom = $pedidos->getOrdersCupom($_POST['order_id']);
        // echo '<pre>';
        // print_r($pedido_detalhes);
        // echo '</pre>';
        echo json_encode(array('1', json_encode($pedido_detalhes), json_encode($pedido_cupom)));
      } else {
        echo json_encode(array('0', '<strong>Erro!</strong> Dados em falta'));
        exit();
      }
    }

    if($action == 'cancelar'){
      if($_POST['dados'][0]['value']){

        $pedido = $pedidos->getOrderById($_POST['dados'][0]['value']);
        $cliente = $users->readUsuario(null, $pedido['id_user']);
        $loja = $lojas->readLojas($pedido['id_loja']);

        //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $html = '<h1>Pedido cancelado, código referência '.$pedido['referencia'].'</h1>';
        $html .= '<p>Lamentamos mas seu pedido foi cancelado.</p>';
        $html .= '<p>Para entender melhor o que ocorreu entre em contacto com a loja no número '.$loja['tel'].'.</p>';

        //email cancelamento de pedido
        SendMail::trySendMail('Pedido cancelado!', $cliente['email'], $html, new PHPMailer(true));
        //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        $pedidos->changeOrderStatus($_POST['dados'][0]['value'], 0, $_POST['dados'][1]['value'], 'usuário');
        echo json_encode(array('1', 'Status do pedido alterado para cancelado'));
        exit();
      } else {
        echo json_encode(array('0', '<strong>Erro!</strong> Dados em falta'));
        exit();
      }
    }

    if($action == 'change'){
      if($_POST['dados'][0]['value']){
        $pedidos->changeOrderStatus($_POST['dados'][0]['value'], $_POST['dados'][1]['value'], null, 'usuário');
        echo json_encode(array('1', 'Status do pedido alterado para cancelado'));
        exit();
      } else {
        echo json_encode(array('0', '<strong>Erro!</strong> Dados em falta'));
        exit();
      }
    }

  }

if($action == 'novos'){
  if($_SESSION['admin']['franqueado']){
    $pedidos_lista = $pedidos->getOrdersByStatus(1, $_SESSION['admin']['franqueado']['id']);
    $automatic_advance = $lojas->getLojaAutomaticAdvance($_SESSION['admin']['franqueado']['id'])['ligado'];
    $options = array('next'=>2, 'cancel'=>true, 'next_page'=>'novos');
  } else {
    $pedidos_lista = $pedidos->getOrdersByStatus(1);
  }
} else if($action == 'producao'){
  if($_SESSION['admin']['franqueado']){
    $pedidos_lista = $pedidos->getOrdersByStatus(2, $_SESSION['admin']['franqueado']['id']);
    $options = array('next'=>3, 'cancel'=>true, 'next_page'=>'entrega');
  } else {
    $pedidos_lista = $pedidos->getOrdersByStatus(2);
  }
} else if($action == 'entrega'){
  if($_SESSION['admin']['franqueado']){
    $pedidos_lista = $pedidos->getOrdersByStatus(3, $_SESSION['admin']['franqueado']['id']);
    $options = array('next'=>4, 'cancel'=>true, 'next_page'=>'finalizados');
  } else {
    $pedidos_lista = $pedidos->getOrdersByStatus(3);
  }
} else if($action == 'finalizados'){
  if($_SESSION['admin']['franqueado']){
    $pedidos_lista = $pedidos->getOrdersByStatus(4, $_SESSION['admin']['franqueado']['id']);
    $options = array('next'=>null, 'cancel'=>false, 'next_page'=>null);
  } else {
    $pedidos_lista = $pedidos->getOrdersByStatus(4);
  }
} else if($action == 'cancelados'){
  if($_SESSION['admin']['franqueado']){
    $pedidos_lista = $pedidos->getOrdersByStatus(0, $_SESSION['admin']['franqueado']['id']);
    $options = array('next'=>null, 'cancel'=>false, 'next_page'=>null);
  } else {
    $pedidos_lista = $pedidos->getOrdersByStatus(0);
  }
}

// if($_SESSION['admin']['id'] == 1){
//   $pedido = $pedidos->getOrderById(1);
//   $cliente = $users->readUsuario(null, $pedido['id_user']);
//   $loja = $lojas->readLojas($pedido['id_loja']);
//
//   //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//   $html = '<h1>Pedido cancelado, código referência '.$pedido['referencia'].'</h1>';
//   $html .= '<p>Lamentamos mas seu pedido foi cancelado.</p>';
//   $html .= '<p>Para entender melhor o que ocorreu entre em contacto a loja no número '.$loja['tel'].'.</p>';
//
//   //email cancelamento de pedido
//   SendMail::trySendMail('Pedido cancelado!', $cliente['email'], $html, new PHPMailer(true));
//   //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
//   echo '<pre>';
//   print_r($cliente);
//   echo '</pre>';
//
// }

// echo '<pre>';
// print_r($pedidos_lista);
// echo '</pre>';

?>
