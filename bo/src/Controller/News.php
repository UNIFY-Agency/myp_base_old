<?php

$news = new News(new Config());

if($action == 'listar'){

    $novidades = $news->readNews();

} else if($action == 'inserir'){

  if(!empty($_POST['title']) || !empty($_POST['conteudo'])){

    $dados['title'] = $_POST['title'];
    $dados['status'] = empty($_POST['status']) ? NULL  : $_POST['status'] ;
    $dados['conteudo'] = $_POST['conteudo'];

    $news->insertNews($dados);
    header('Location: '.URL_BASE.DS.$module.'/listar');

  }

} else if($action == 'deletar'){

  $categoria = $cat->readCat($param);

  if($_SERVER['REQUEST_METHOD']=='POST'){

    $cat->deleteCat($param);
    header('Location: ../listar');

  }

} else if($action == 'editar'){

  $novidade = $news->readNews($param);

  if(!empty($_POST['title']) || !empty($_POST['conteudo'])){

    $dados['id'] = $param;
    $dados['title'] = $_POST['title'];
    $dados['status'] = empty($_POST['status']) ? NULL  : $_POST['status'] ;
    $dados['conteudo'] = $_POST['conteudo'];

    $news->editNews($dados);
    header('Location: '.URL_BASE.DS.$module.'/listar');

  }

}

// echo '<pre>';
// print_r($_POST);
// echo '</pre>';

?>
