<?php

$cupom = new Cupom(new Config());
$mensagem_erro = false;

if (Request::isAjax()){
  if($action == 'ativar'){
    $cupom->ativaCupom($_POST['id']);
  } else if($action == 'inativar'){
    $cupom->inativaCupom($_POST['id']);
  }
}

if($action == 'listar'){

  $cupom_lista = $cupom->readCupom();

  // echo '<pre>';
  // print_r($lojas_lista);
  // echo '</pre>';

} else if($action == 'inserir'){

  $loader->get('src/Model/Lojas');
  $lojas = new Lojas(new Config());
  $lojas_lista = $lojas->listarLojasAtivas();

  if($_POST){
    if(!empty($_POST['nome']) && !empty($_POST['codigo']) && !empty($_POST['tipo']) && !empty($_POST['valor'])){

      $dados['nome'] = $_POST['nome'];
      $dados['codigo'] = $_POST['codigo'];
      $dados['tipo'] = $_POST['tipo'];
      $dados['valor'] = $_POST['valor'];

      $result = $cupom->insertCupom($dados);
      if(substr($result, 0, 4) == 'erro'){
        switch ($result) {
            case 'erro':
              $mensagem_erro = '<strong>Erro!</strong> Esse código já está cadastrado no sistema!';
              break;
        }
      } else {
        $dados['LOJAS'] = array();
        foreach ($lojas_lista as $key => $value) {
            if (array_key_exists($value['id'], $_POST)){
              $cupom->linktCupomToLoja($result, $value['id']);
            }
        }

        header('Location: '.URL_BASE.'/cupom/listar');
      }

    } else {
      $mensagem_erro = '<strong>Erro!</strong> Algum dado obrigatório ficou em falta!';
    }
  }

} else if($action == 'deletar'){

  $categoria = $cat->readCat($param);

  if($_SERVER['REQUEST_METHOD']=='POST'){

    $cat->deleteCat($param);
    header('Location: ../listar');

  }

} else if($action == 'editar'){

  if(empty($param)){ header('Location: '.URL_BASE.'/404'); }

  $cupom_editar = $cupom->readCupom($param);

  if($_POST){
    if(!empty($_POST['nome']) && !empty($_POST['codigo']) && !empty($_POST['tipo']) && !empty($_POST['valor'])){

      $dados['nome'] = $_POST['nome'];
      $dados['codigo'] = $_POST['codigo'];
      $dados['tipo'] = $_POST['tipo'];
      $dados['valor'] = $_POST['valor'];

      $result = $cupom->editCupom($dados, $cupom_editar['id']);
      if(substr($result, 0, 4) == 'erro'){
        switch ($result) {
            case 'erro':
              $mensagem_erro = '<strong>Erro!</strong> Esse código já está cadastrado em outro registro!';
              break;
        }
      } else {
        header('Location: '.URL_BASE.'/cupom/listar');
      }

    } else {
      $mensagem_erro = '<strong>Erro!</strong> Algum dado obrigatório ficou em falta!';
    }
  }

} else if($action == 'pesquisa'){

  if(!empty($_POST['codigo'])){

    $endereco = $enderecos->buscarCP($_POST['codigo']);

    if(empty($endereco)){
      $mensagem_erro = '<strong>Erro!</strong> Não existe nenhum código como esse cadastrado no sistema!';
    } else { header('Location: '.URL_BASE.'/enderecos/editar/'.$endereco['id']); }

  }

}

// echo '<pre>';
// print_r($_POST);
// echo '</pre>';

?>
