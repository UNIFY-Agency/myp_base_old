<?php

$loader->get('src/Model/Anuncios');
$anuncios = new Anuncios(new Config());

$mensagens = new Mensagens(new Config());

if($action == 'listar'){

  if($_SESSION['usuario']['tipo'] == 1){
    $meus_anuncios = $anuncios->readMyAnunciosId($_SESSION['usuario']['id']);
    $meus_anuncios_id = array_column($meus_anuncios, 'id');
    $messages = $mensagens->readMyQuests(implode(',',$meus_anuncios_id));
  } else if($_SESSION['usuario']['tipo'] == 2){
    $meus_anuncios_id = array();
    $messages = $mensagens->readMessages(null, null, $_SESSION['usuario']['id']);
  }

}

if($_SERVER['REQUEST_METHOD']=='POST' &&  @$_POST['dados']){
  $dados['anuncio'] = $_POST['dados'][0];
  $dados['mensagem'] = $_POST['dados'][1];
  $dados['para'] = $_POST['dados'][2];

  $mensagens->makeAnswer($dados);
}

// echo '<pre>';
// print_r($messages);
// echo '</pre>';

?>
