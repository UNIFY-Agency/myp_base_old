<?php

  $marcas = new Marcas(new Config());
  $mensagem_erro = false;

if($action == 'listar'){

    $marcas_lista = $marcas->listarMarcasPaises();

    $marcas_combine = array();
    foreach ($marcas_lista as $element) {
        $marcas_combine[$element['id']][] = $element;
    }

} else if($action == 'inserir_antigo'){

  $paises = $marcas->getPaisesAtivos();

  if($_POST){
    if(!empty($_POST['nome']) and !empty($_POST['segmento']) and !empty($_POST['codigo'])){

        $dados['nome'] = $_POST['nome'];
        $dados['segmento'] = $_POST['segmento'];
        $dados['codigo'] = strtoupper($_POST['codigo']);
        $dados['descricao'] = empty($_POST['descricao']) ? '' : $_POST['descricao'] ;

        $marca_id = $marcas->insertMarca($dados);
        if(substr($marca_id, 0, 4) == 'erro'){
          switch ($marca_id) {
              case 'erro':
                $mensagem_erro = '<strong>Erro!</strong> Esse código já está sendo usado por uma marca!';
                break;
          }
        } else {
          $marcas->cadastrarMarcaPais($marca_id, $key, $value);
          header('Location: '.URL_BASE.'/marcas/listar');
        }

    } else {
      $mensagem_erro = '<strong>Erro!</strong> Algum dado obrigatório ficou em falta!';
    }
  }

  // echo '<pre>';
  // print_r($dados);
  // echo '</pre>';

}  else if($action == 'inserir'){

    // echo '<pre>';
    // print_r($_POST);
    // echo '</pre>';
    // echo '<pre>';
    // print_r($_FILES);
    // echo '</pre>';

    if($_POST){
      if(!empty($_POST['nome'])){

          $foto_name = date('dmyHis').sha1($_FILES['logo']['name']).'.'.strtolower(pathinfo($_FILES['logo']['name'],PATHINFO_EXTENSION));

          $dados['nome'] = $_POST['nome'];
          $dados['descricao'] = $_POST['descricao'];
          $dados['catalogo'] = $_POST['catalogo'];
          $dados['logo'] = $foto_name;
          $dados['foto'] = $_SERVER['DOCUMENT_ROOT']."/bo/assets/upload/$foto_name";

          $uploadOk = 1;
          $target_file = $dados['logo'];
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

          echo '<pre>';
          print_r($dados);
          echo '</pre>';
          die();


          //$check = getimagesize($_FILES["logo"]["tmp_name"]);
          // if(!$check) {
              // $mensagem_erro = "O arquivo enviado não é uma imagem.";
              // $uploadOk = 0;
          //} else
          if (file_exists($target_file)) {
              $mensagem_erro = "Desculpe esse arquivo já existe.";
              $uploadOk = 0;
          } else if ($_FILES["logo"]["size"] > 5242880) { // 1048576 = 1MB // 5242880 = 5MB
              $mensagem_erro = "Desculpe, esse arquivo é muito grande.";
              $uploadOk = 0;
          } else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" && $imageFileType != "pdf" ) {
              $mensagem_erro = "O sistema aceita somente JPG, JPEG, PNG, GIF e PDF.";
              $uploadOk = 0;
          } else if ($uploadOk == 0) {
              $mensagem_erro = "Desculpe, seu arquivo não foi enviado.";
          } else {
            if (move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file)) {
              $result = $marcas->insertMarcaEurotecnologia($dados);
              if(substr($result, 0, 4) == 'erro'){
                switch ($result) {
                    case 'erro':
                      $mensagem_erro = '<strong>Erro!</strong> Houve algum erro interno!';
                      break;
                }
              } else {
                header('Location: '.URL_BASE.'/produtos/listar');
              }
            } else {
                $mensagem_erro = "Desculpe, houve algum erro e seu arquivo não foi enviado.";
            }
          }
      } else {
        $mensagem_erro = '<strong>Erro!</strong> Algum dado obrigatório ficou em falta!';
      }
    }

  } else if($action == 'editar'){

  if(empty($param)){ header('Location: '.URL_BASE.'/404'); }

  $marca = $marcas->listarMarcaPaises($param);
  $paises = $marcas->getPaisesAtivos();

  if($_POST){
    if(!empty($_POST['nome']) and !empty($_POST['segmento']) and !empty($_POST['codigo'])){

        $dados['nome'] = $_POST['nome'];
        $dados['segmento'] = $_POST['segmento'];
        $dados['codigo'] = strtoupper($_POST['codigo']);
        $dados['descricao'] = empty($_POST['descricao']) ? '' : $_POST['descricao'] ;

        $dados['PAISES'] = array();
        foreach ($paises as $key => $value) {
            if (array_key_exists($value['id'], $_POST)){
              $dados['PAISES'][$value['id']] = $_POST[$value['id']] == 'on' ? 1 : 0 ;
            } else {
              $dados['PAISES'][$value['id']] = 0 ;
            }
        }

        $atualiza = $marcas->editMarca($param, $dados);
        if(substr($atualiza, 0, 4) == 'erro'){
          switch ($atualiza) {
              case 'erro':
                $mensagem_erro = '<strong>Erro!</strong> Esse código já está sendo usado por uma marca!';
                break;
          }
        } else {
          foreach ($dados['PAISES'] as $key => $value) {
            $marcas->atualizaMarcaPais($param, $key, $value);
          }
          header('Location: '.URL_BASE.'/marcas/listar');
        }

    } else {
      $mensagem_erro = '<strong>Erro!</strong> Algum dado obrigatório ficou em falta!';
    }
  }
} else if($action == 'ver'){

  if(empty($param)){ header('Location: '.URL_BASE.'/404'); }

  $marca = $marcas->readMarcas($param);
  $paises = array_column($marcas->getPaisesMarca($param), 'pais');

}

?>
