<?php

$users = new Users(new Config());

if($action == 'listar'){

    $usuarios = $users->readUsuario();

} else if($action == 'atualizar'){

  if(empty($param)) { echo "<h1>Não é possível atualizar sem ID!</h1>"; die; }

  if(!empty($_POST['email']) and !empty($_POST['password'])){

      $dados['nome'] = (empty($_POST['nome'])) ? '' : $_POST['nome'] ;
      $dados['email'] = $_POST['email'];
      $dados['usuario'] = (empty($_POST['usuario'])) ? '' : $_POST['usuario'];
      $dados['senha'] = $_POST['password'];

      $users->updateUsuario($dados, $param);
      header('Location:../listar');
  } else {
    $usuarios = $users->readUsuario(null, $param);
  }

} else if($action == 'inserir'){

  if(!empty($_POST['email']) and !empty($_POST['password'])){

      $users->cadastrar($_POST);
      header('Location: listar');

  }

} else if($action == 'deletar'){

  if(empty($param)) { echo "<h1>Não é possível deletar sem ID!</h1>"; die; }

  $usuario = $users->readUsuario(null, $param);

  if($_SERVER['REQUEST_METHOD'] == 'POST'){
      $users->deleteUsuario($param);
      header('Location:../listar');
  }

}
