<?php

$orders = new Ordens(new Config());

$loader->get('src/Model/Empresa');
$empresas = new Empresa(new Config());

$loader->get('src/Model/Anuncios');
$anuncios = new Anuncios(new Config());

$loader->get('src/Model/Lances');
$lances = new Lances(new Config());

if($action == 'listar'){

    $ordens = $orders->readOrders(null, null, null, ', lances.pai as lance_pai, lances.id as lance_id, lances.status as status');

} else if($action == 'espera'){

  //$orders->getWait($_SESSION['usuario']['id'], $param);
  //header('Location: '.URL_BASE.'/ordens/listar');

} else if($action == 'pagar'){

    if (Request::isAjax()){
    
        $result = $orders->pay($param, $_POST['details']['id'], json_encode($_POST));            
  
    } else {
    
        $result = $orders->pay($param, $_SESSION['pagamento_espera']['pagseguro_ref'], json_encode($_SESSION['pagamento_espera']));
        unset($_SESSION['pagamento_espera']);
        $url = "https://a-zservicos.com.br/sys/ordens/ver/".$_SESSION['pagamento_espera']['codigo'];
        echo "<script>window.location.replace('$url');</script>";
    
    }

} else if($action == 'ver'){

  $google_maps = true;

  $lance = $lances->readLances(null, $param);
  $publication = $anuncios->readAnuncios(null, $lance['anuncio']);
  $company = $empresas->readUsuario(null, $lance['pai']);
  $dados_comp = $empresas->companyData($company['id']);
  $cliente = $users->readUsuario(null, $publication['pai']);

  if (Request::isAjax()){

    $lances->putWait($param);

  }

}

// echo '<pre>';
// print_r($ordens);
// echo '</pre>';

?>
