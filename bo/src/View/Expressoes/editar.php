<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3><?php echo $module; ?> <small>Editar <?php echo $module.' '.$expression['id']; ?>.</small></h3>
      </div>

      <?php include "src/View/Includes/search.php"; ?>

    </div>

    <div class="clearfix"></div>

    <?php if($mensagem_erro): ?>

      <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <?php echo $mensagem_erro; ?>
      </div>

    <?php endif; ?>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-list"></i> Formulário: Editar <?php echo $module.' '.$expression['pt']; ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Configurações 1</a>
                  </li>
                  <li><a href="#">Configurações 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="edit_exp_form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="">

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Conteúdo <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea class="form-control" name="pt" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">
                    <?php echo $expression['pt']; ?>
                  </textarea>
                </div>
              </div>
              <div class="form-group hidden">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Inglês
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="en" class="form-control col-md-7 col-xs-12" value="<?php echo $expression['en']; ?>">
                </div>
              </div>
              <div class="form-group hidden">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Espanhol
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="es" class="form-control col-md-7 col-xs-12" value="<?php echo $expression['es']; ?>">
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <a href="<?php echo URL_BASE; ?>/expressoes/listar" class="btn btn-primary" type="button">Voltar</a>
		              <a href="" class="btn btn-primary" type="reset">Limpar</a>
                  <input type="hidden" name="id" value="<?php echo $expression['id']; ?>">
                  <input class="btn btn-success" type="submit" name="submit" value="Enviar">
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
