
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-xs-12">
            <form action="" method="post">

                <h2>Remover o Usuario  de ID #<?php echo $param; ?> e de nome <?php echo $usuario['nome']; ?></h2>

                <hr/>

                <div class="input-group">
                    <input class="btn btn-primary" type="submit" value="Sim">
                    <a class="btn btn-danger" href="../listar">Não</a>
                </div>

            </form>

        </div>
    </div>
</div>
