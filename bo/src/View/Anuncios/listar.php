<div class="container">
    <div class="row">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-user"></span> Anuncios </h3>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered table-condensed">
                    <thead>
                    <tr>
                        <th><?php if($_SESSION['usuario']['tipo'] == 0){ echo "Pai"; } else { echo "Ações"; } ?></th>
                        <th>Título</th>
                        <th>Endereço</th>
                        <th>Descrição</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if($publications): ?>
                    <?php foreach($publications as $publication): ?>
                    <?php $pai = $users->readUsuario(null, $publication['pai']);
                    ?>
                        <tr>
                            <?php if(empty($pai['nome'])){ echo "<td style='color: red;'>Usuário sem nome</td>"; }else{ echo '<td>'.$pai['nome'].'</td>'; } ?>
                            <td><?php echo $publication['titulo']; ?></td>
                            <td><?php echo $publication['endereco']; ?></td>
                            <td><?php if(strlen($publication['descricao']) > 100){ echo substr($publication['descricao'],0,100) . "...";} else {echo $publication['descricao'];} ?></td>
                            <td>
                              <!-- <a class="btn btn-danger" href="index.php?pages=deletecar&id=<?php //echo $publication['id']; ?>">Remover</a> -->
                              <a class="btn btn-primary" href="<?php echo URL_BASE; ?>/anuncios/ver/<?php echo $publication['id']; ?>">Ver</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
