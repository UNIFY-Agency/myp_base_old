<div class="container zero-form" id="crop-avatar">
    <div class="row">

        <div class="panel panel-default">
            <div class="panel-heading col-xs-11">
              <h3 class="panel-title"><span class="glyphicon glyphicon-tag"></span> <?php echo $publication['titulo']; ?> </h3>
            </div>
            <div class="panel-heading col-xs-1">
              <a class="btn btn-danger pull-right" href="<?php echo URL_BASE; ?>/anuncios/listar">Voltar</a>
            </div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-4">
                  <!-- <div class="thumbnail avatar-view" onClick="refleshIMG()">
                      <img src="<?php //echo URL_BASE; ?>/assets/images/logo.png" alt="Avatar">
                      <div class="caption">
                      </div>
                  </div> -->
                  <div class="thumbnail avatar-view" id="map" style="height: 400px; width: 100%;">
                    <input type="hidden" id="cepspan" value="<?php echo $publication['endereco']; ?>">
                  </div>
                  <div class="input-group">
                      <span class="input-group-addon zero-addon"><strong>Execução:</strong></span>
                      <span class="input-group-addon zero-addon"><?php echo strftime('%d/%m/%Y', strtotime($publication['dia'])); ?></span>
                  </div>
                  <div class="input-group">
                      <span class="input-group-addon zero-addon"><strong>Publicação:</strong></span>
                      <span class="input-group-addon zero-addon"><?php echo strftime('%d/%m/%Y', strtotime($publication['inclusao'])); ?></span>
                  </div>
                  <div class="input-group">
                      <span class="input-group-addon zero-addon"><strong>Expiração:</strong></span>
                      <span class="input-group-addon zero-addon"><?php echo strftime('%d/%m/%Y', strtotime($publication['validade'])); ?></span>
                  </div>
                  <div class="">
                      <span class="input-group-addon"><strong>Descrição:</strong></span>
                      <p><?php echo $publication['descricao']; ?></p>
                  </div>
                </div>

                <div class="col-md-8 col-xs-12">

                  <h3 class="panel-title text-center"><span class="glyphicon glyphicon-user"></span> Lances </h3>
                  <hr/>

                  <div class="table-responsive">
                      <table class="table table-striped table-hover table-bordered table-condensed">
                          <thead>
                          <tr>
                              <th>Pai</th>
                              <th>Valor</th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php if($lances_lista): ?>
                          <?php foreach($lances_lista as $lance): ?>
                          <?php if($lance['publicado'] == 1){ $ativ_val = array('btn-success', 'glyphicon-ok', 0); }else{ $ativ_val = array('btn-warning', 'glyphicon-remove', 1); } ?>
                          <?php $pai = $users->readUsuario(null, $lance['pai']); ?>
                              <tr>
                                  <td><a href="../../empresa/ver/<?php echo $lance['pai']; ?>"><?php echo $pai['nome']; ?></a></td>
                                  <td>
                                    <?php
                                      if($_SESSION['usuario']['tipo'] == 1){
                                        //porcentagem do site 20%
                                        echo "R$ ".number_format(($lance['valor'] + ((20 / 100) * $lance['valor'])), 2, ',', '.');
                                      } else {
                                        echo "R$ ".number_format($lance['valor'], 2, ',', '.');
                                      }
                                    ?>

                                  <?php if($_SESSION['usuario']['tipo'] == 1): ?>
                                    <a href="<?php echo URL_BASE; ?>/anuncios/fechar/<?php echo $lance['id']; ?>" class="btn btn-primary pull-right">Fechar</a>
                                  <?php endif; ?>
                                  </td>
                              </tr>
                          <?php endforeach; ?>
                        <?php endif; ?>
                          </tbody>
                      </table>
                  </div>

                  <?php if($_SESSION['usuario']['tipo'] != 1): ?>

                  <div class="featured-box featured-box-primary align-left mt-xlg">
                      <div class="box-content">
                          <form action="" id="lanceMaker" method="post">
                              <div class="row">
                                  <div class="col-md-7">
                                  </div>
                                  <div class="col-md-3">
                                    <div class="form-group">
                                      <input type="text" name="valor" value="" class="form-control" placeholder="Digite aqui o valor" onkeyup="substituiVirgula(this)">
                                    </div>
                                  </div>
                                  <div class="col-md-2">
                                    <div class="form-group">
                                      <input type="submit" value="Enviar" class="btn btn-primary" data-loading-text="Loading..." onclick="return confirm('Ao confimrar você se compromete a fazer o serviço que pede no valor que estipulou no campo ao lado')">
                                    </div>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>

                  <?php endif; ?>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-picture"></span> Perguntas </h3>
            </div>
            <div class="panel-body">

              <div class="table-responsive">
                  <table class="table table-striped table-hover table-bordered table-condensed">
                      <thead>
                      <tr>
                          <th>Usuário</th>
                          <th>Pergunta</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php if($messages): $empresas = array(); ?>
                      <?php foreach($messages as $message): ?>
                      <?php if (in_array($message['pai'], $empresas)) { continue; }  ?>
                      <?php if($message['publicado'] == 1){ $ativ_val = array('btn-success', 'glyphicon-ok', 0); }else{ $ativ_val = array('btn-warning', 'glyphicon-remove', 1); } ?>
                      <?php $pai = $users->readUsuario(null, $message['pai']); ?>
                      <?php $answer = $mensagens->getAnswer($message['id']); ?>
                          <tr>
                              <td><?php echo $message['mensagem']; ?></td>
                              <td><a href="../../empresa/ver/<?php echo $message['pai']; ?>"><?php echo $pai['nome']; ?></a></td>
                          </tr>
                      <?php if($answer): ?>
                          <tr>
                              <td><?php echo $answer->mensagem; ?></td>
                              <td style="color: green;">Resposta</td>
                          </tr>
                      <?php endif; ?>
                      <?php $empresas[] = $message['pai']; ?>
                      <?php endforeach; ?>
                    <?php endif; ?>
                      </tbody>
                  </table>
              </div>

              <?php if($_SESSION['usuario']['tipo'] == 2): ?>

                  <div class="featured-box featured-box-primary align-left mt-xlg">
                      <div class="box-content">
                          <form action="" id="messageMaker" method="post">
                              <div class="row">
                                  <div class="col-md-10">
                                    <div class="form-group">
                                      <textarea class="form-control" name="mensagem" id="" cols="30" rows="3" placeholder="Faça sua pergunta aqui!"></textarea>
                                    </div>
                                  </div>
                                  <div class="col-md-2">
                                    <div class="form-group">
                                      <input type="submit" value="Enviar" class="btn btn-primary" data-loading-text="Loading...">
                                    </div>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>

                  <?php endif; ?>

            </div>
        </div>
    </div>

    <!-- Loading state -->
    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>

</div>
