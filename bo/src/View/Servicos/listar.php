<div class="container">
    <div class="row">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-user"></span> Serviços </h3>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered table-condensed">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Categoria</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($servicos as $servico): ?>
                    <?php $categoria = $cat->readCat($servico['categoria']); ?>
                        <tr>
                            <td><?php echo $servico['id']; ?></td>
                            <td><?php echo $servico['nome']; ?></td>
                            <td><?php echo $categoria['nome']; ?></td>
                            <td>
                                <a class="btn btn-primary" href="<?php echo URL_BASE; ?>/servicos/editar/<?php echo $servico['id']; ?>">Editar</a>
                                <!-- <a class="btn btn-danger" href="<?php echo URL_BASE; ?>/categorias/deletar/<?php echo $servico['id']; ?>">Remover</a> -->
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
