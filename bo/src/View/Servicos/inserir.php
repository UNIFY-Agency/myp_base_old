<div class="container zero-form">
    <div class="row">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-user"></span> Novo Serviço </h3>
            </div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-4">
                    <div class="thumbnail">
                        <img src="<?php echo URL_BASE; ?>/assets/images/logo.png" alt="Avatar">
                        <div class="caption">
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12">
                    <form action="" method="POST">

                        <div class="input-group">
                            <span class="input-group-addon zero-addon">Categoria:</span>
                            <select class="form-control select" name="categoria">
                              <?php foreach($categorias as $categoria): ?>
                                  <option value="<?php echo $categoria['id']; ?>"><?php echo $categoria['nome']; ?></option>
                              <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon zero-addon">Nome:</span>
                            <input class="form-control" type="text" name="nome">
                        </div>

                </div>
                <div class="col-md-4 col-xs-12">

                        <div class="input-group">
                            <input class="btn btn-primary" type="submit" name="" value="Enviar">
                            <a class="btn btn-danger" href="<?php echo URL_BASE; ?>/servicos/listar">Voltar</a>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
