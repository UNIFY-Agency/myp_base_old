<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="view-modal">Pedido id <span class="view-order-id"></span> referência <span class="view-order-ref"></span></h4>
      </div>
      <div class="modal-body">
        <h4>Pedido do cliente <span class="view-client-name"></span>, Feito às <span class="view-order-time"></span> do dia <span class="view-order-date"></span>.</h4>
        <p>Contato: <span class="view-order-contato"></span></p>
        <p class="view-order-email"></p>
        <p>Endereço: <span class="view-order-location"></span></p>
        <p class="view-order-endereco"></p>
        <p>Fatura: <span class="view-order-fatura"></span></p>
        <p>Forma de pagamento: <span class="view-order-pag"></span></p>
        <strong><p>Pedido <span class="view-order-tipo-entrega"></span></p></strong>
        <span class="view-alergias hidden"></span>
        <span class="view-cupom hidden"></span>
        <p>Produtos.</p>
        <div class="x_content">
          <ul class="list-unstyled msg_list cart-list">
          </ul>
          <br><br>
          <h4><span class="view-order-quant"></span> produtos que custaram <span class="view-order-subtotal"></span>€ + taxa de entrega <span class="view-order-entrega"></span> custaram um total de <span class="view-order-total"></span>€</h4>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-danger cancel-order <?php echo $options['cancel'] ? '' : 'hidden' ; ?>" data-dismiss="modal">Cancelar Pedido</button>
        <button type="button" class="btn btn-primary advance-order <?php echo is_null($options['next']) ? 'hidden' : '' ; ?>" next="<?php echo $options['next']; ?>" action="<?php echo $options['next_page']; ?>">Avançar Pedido</button>
      </div>

    </div>
  </div>
</div>
