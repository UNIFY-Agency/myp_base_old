<?php

class Anuncios{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    //FOTOS

    public function searchGhostFotos() {
        $stmt = $this->mysql->prepare("SELECT f.local FROM fotos f WHERE f.id_pai not in (select distinct(id) from anuncios )");
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function killGhosts() {
        $foto = $this->searchGhostFotos();
        foreach($foto as $fotos){ unlink($fotos['local']); }

        $kill = $this->mysql->prepare("DELETE FROM fotos WHERE id_pai not in (select distinct(id) from anuncios )");
        $kill->execute();
    }

    public function cadastrarFoto($nome, $id=null){

        if(empty($id)) {
            $stmt = $this->mysql->prepare("SELECT id FROM anuncios ORDER BY id DESC LIMIT 1");
            $stmt->execute();
            $result = $stmt->fetch();

            $id = $result['id'] + 1;
        }

        $cadastra = $this->mysql->prepare('INSERT into fotos (id_pai, local, inclusao) VALUES (:id_pai, :local, :inclusao);');
        $cadastra->bindValue(':id_pai', $id, PDO::PARAM_STR);
        $cadastra->bindValue(':local', $nome, PDO::PARAM_STR);
        $cadastra->bindValue(':inclusao', date('Y-m-d'), PDO::PARAM_STR);
        $cadastra->execute();

    }

    public function searchFoto($id) {
        $stmt = $this->mysql->prepare("SELECT local FROM fotos WHERE id_pai = :id");
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function deleteFoto($id=null, $local=null, $ajax_ajust=null){
        if(!empty($id)) {
            $delete = $this->mysql->prepare('DELETE FROM fotos WHERE id = :id;');
            $delete->bindValue(':id', $id, PDO::PARAM_INT);
            $delete->execute();

            $foto = $this->searchFoto($id);
            $endereco = $ajax_ajust.$fotos['local'];
            unlink($endereco);
        } else if(!empty($local)) {
            $delete = $this->mysql->prepare('DELETE FROM fotos WHERE local = :local;');
            $delete->bindValue(':local', $local, PDO::PARAM_INT);
            $delete->execute();

            $endereco = $ajax_ajust.$local;
            unlink($endereco);
        } else {}
    }

    public function publicar($ativ, $id){
        $change = ($ativ == 1) ? 0 : 1;

        $update = $this->mysql->prepare('UPDATE anuncios SET publicado = :publicado WHERE id = :id;');
        $update->bindValue(':publicado',         $change, PDO::PARAM_STR);
        $update->bindValue(':id',           $id, PDO::PARAM_INT);

        return $update->execute();
    }

    //CRUD

    // public function cadastrarAnuncio($dados){
    //
    //     if($_SERVER['REQUEST_METHOD']=='POST'){
    //         $pai = (isset($_SESSION)) ? $_SESSION['usuario']['id'] : false ;
    //
    //         if(!$pai){
    //           $pai = $dados['pai'];
    //         }
    //
    //         $cadastra = $this->mysql->prepare('INSERT into anuncios (titulo, valor, descricao, inclusao, ativ, pai, validade) VALUES (:titulo, :valor, :descricao, :inclusao, 1, '.$pai.', :validade);');
    //         $cadastra->bindValue(':titulo', $dados['titulo'], PDO::PARAM_STR);
    //         $cadastra->bindValue(':valor', $dados['valor'], PDO::PARAM_STR);
    //         $cadastra->bindValue(':descricao', $dados['descricao'], PDO::PARAM_STR);
    //         $cadastra->bindValue(':inclusao', date('Y-m-d'), PDO::PARAM_STR);
    //         $cadastra->bindValue(':validade', date('Y').'-'.$dados['mes'].'-'.$dados['dia'], PDO::PARAM_STR);
    //         $cadastra->execute();
    //         //header('Location:index.php?pages=anuncio');
    //     }
    // }

    public function cadastrarAnuncio($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO anuncios (`titulo`, `categoria`, `servico`, `descricao`, `endereco`, `validade`, `urgente`, `dia`, `inclusao`, `ativ`, `pai`) VALUES (:titulo, :categoria, :servico, :descricao, :endereco, :validade, :urgente, :dia, :inclusao, 1, :pai);');
            $cadastra->bindValue(':titulo', $dados['titulo'], PDO::PARAM_STR);
            $cadastra->bindValue(':categoria', $dados['categoria'], PDO::PARAM_INT);
            $cadastra->bindValue(':servico', $dados['servico'], PDO::PARAM_INT);
            $cadastra->bindValue(':descricao', $dados['descricao'], PDO::PARAM_STR);
            $cadastra->bindValue(':endereco', $dados['local1cep'], PDO::PARAM_INT);
            //$cadastra->bindValue(':destino', $dados['local2cep'], PDO::PARAM_INT);
            $cadastra->bindValue(':validade', $dados['tempo'], PDO::PARAM_INT);
            $cadastra->bindValue(':urgente', $dados['urgente'], PDO::PARAM_INT);
            $cadastra->bindValue(':dia', $dados['diaServico'], PDO::PARAM_STR);
            $cadastra->bindValue(':inclusao', date('Y-m-d'), PDO::PARAM_STR);
            $cadastra->bindValue(':pai', $dados['pai'], PDO::PARAM_INT);
            $cadastra->execute();
            
            if($dados['local2cep']){
            
              $last = $this->mysql->lastInsertId();
              
              $cadastra = $this->mysql->prepare('INSERT INTO `anuncios_destino` (`cep`, `cidade`, `UF`, `numero`, `complemento`, `anuncio_pai`) VALUES (:cep, :cidade, :UF, :numero, :complemento, :anuncio_pai);');
              $cadastra->bindValue(':cep', $dados['local2cep'], PDO::PARAM_INT);
              $cadastra->bindValue(':cidade', $dados['cidade2'], PDO::PARAM_STR);
              $cadastra->bindValue(':UF', $dados['UF2'], PDO::PARAM_STR);
              $cadastra->bindValue(':numero', $dados['numero2'], PDO::PARAM_STR);
              $cadastra->bindValue(':complemento', $dados['complemento2'], PDO::PARAM_STR);
              $cadastra->bindValue(':anuncio_pai', $last, PDO::PARAM_STR);
              $cadastra->execute();
            }
        }
    }

    public function readAnuncios($nome=null, $id=null, $pai=null){
        if(!empty($nome)) {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE titulo = :nome AND ativ = 1;');
            $select->bindValue(':nome', $nome, PDO::PARAM_STR);
            $select->execute();
            return $select->fetch();
        } else if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE id = :id AND ativ = 1;');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($pai)) {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE pai = :id AND ativ = 1;');
            $select->bindValue(':id', $pai  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE ativ = 1;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function readMyAnunciosId($pai){
        $select = $this->mysql->prepare('SELECT id FROM anuncios WHERE pai = :id AND ativ = 1;');
        $select->bindValue(':id', $pai  , PDO::PARAM_INT);
        $select->execute();
        return $select->fetchAll();
    }

    public function readPubAnuncio($nome=null, $id=null){
        if(!empty($nome)) {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE titulo = :nome AND ativ = 1 AND publicado = 1;');
            $select->bindValue(':nome', $nome, PDO::PARAM_STR);
            $select->execute();
            return $select->fetch();
        } else if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE id = :id AND ativ = 1 AND publicado = 1;');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE ativ = 1 AND publicado = 1;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    //readAllAnuncios existe para ler até os anuncios inativos
    public function readAllAnuncios($nome=null, $id=null){
        if(!empty($nome)) {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE titulo = :nome;');
            $select->bindValue(':nome', $nome, PDO::PARAM_STR);
            $select->execute();
            return $select->fetch();
        } else if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE id = :id;');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else {
            $select = $this->mysql->prepare('SELECT * FROM anuncios;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    
    public function complementAnuncio($id, $dados){
        $select = $this->mysql->prepare('SELECT lances.id, anuncios.id FROM lances INNER JOIN anuncios ON lances.anuncio = anuncios.id WHERE lances.id = :id;');
        $select->bindValue(':id', $id , PDO::PARAM_INT);
        $select->execute();
        $resultado = $select->fetch(PDO::FETCH_ASSOC);
    
        $update = $this->mysql->prepare('UPDATE anuncios SET numero = :numero, complemento = :complemento, tel = :tel WHERE id = :id;');
        $update->bindValue(':numero',       $dados[8]['value'], PDO::PARAM_INT);
        $update->bindValue(':complemento',  $dados[9]['value'], PDO::PARAM_STR);
        $update->bindValue(':tel',          $dados[6]['value'], PDO::PARAM_STR);
        $update->bindValue(':id',           $resultado['id'], PDO::PARAM_INT);
        return $update->execute();
    }
            
    public function updateAnuncio($dados, $id){
        $update = $this->mysql->prepare('UPDATE anuncios SET titulo = :titulo, valor = :valor, descricao = :descricao, inclusao = :inclusao, ativ = :ativ WHERE id = :id;');
        $update->bindValue(':titulo',       $dados['titulo'], PDO::PARAM_STR);
        $update->bindValue(':valor',        $dados['valor'], PDO::PARAM_STR);
        $update->bindValue(':descricao',    $dados['descricao'], PDO::PARAM_STR);
        $update->bindValue(':descricao',    $dados['descricao'], PDO::PARAM_STR);
        $update->bindValue(':inclusao',     date('Y-m-d'), PDO::PARAM_STR);
        $update->bindValue(':ativ',         1, PDO::PARAM_STR);
        $update->bindValue(':id',           $id, PDO::PARAM_INT);

        if($update->execute()){
            return 'foi';
        }
    }

    public function deleteAnuncio($id){
        $delete = $this->mysql->prepare('UPDATE anuncios SET ativ = 0 WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        $delete->execute();

        $foto = $this->searchFoto($id);
        foreach($foto as $fotos){ unlink($fotos['local']); }

        $deletef = $this->mysql->prepare('DELETE FROM fotos WHERE id_pai = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
