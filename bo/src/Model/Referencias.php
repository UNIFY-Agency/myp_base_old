<?php

class Referencias{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    //CRUD

    public function insertRef($dados, $pai){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO referencias (pai, empresa, telefone, email, servico) VALUES (:pai, :empresa, :telefone, :email, :servico);');
            $cadastra->bindValue(':pai', $pai, PDO::PARAM_INT);
            $cadastra->bindValue(':empresa', $dados['nome'], PDO::PARAM_STR);
            $cadastra->bindValue(':telefone', $dados['tele'], PDO::PARAM_STR);
            $cadastra->bindValue(':email', $dados['mail'], PDO::PARAM_STR);
            $cadastra->bindValue(':servico', $dados['serv'], PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readRef($id=null, $name=null, $pai=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM referencias WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM referencias WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else if(!empty($pai)) {
            $select = $this->mysql->prepare('SELECT * FROM referencias WHERE pai = :pai');
            $select->bindValue(':pai', $pai , PDO::PARAM_INT);
            $select->execute();
            return $select->fetchAll();
        }else{
            $select = $this->mysql->prepare('SELECT * FROM referencias WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCat($dados){
        $deletef = $this->mysql->prepare('UPDATE referencias SET nome = :nome WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM referencias WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
