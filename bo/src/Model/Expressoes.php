<?php

class Expressoes{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    //CRUD

    public function insertExp($dados){
      $cadastra = $this->mysql->prepare('INSERT INTO exp (pt, en, es) VALUES (:pt, :en, :es);');
      $cadastra->bindValue(':pt', $dados['pt'], PDO::PARAM_STR);
      $cadastra->bindValue(':en', $dados['en'], PDO::PARAM_STR);
      $cadastra->bindValue(':es', $dados['es'], PDO::PARAM_STR);
      $cadastra->execute();
    }

    public function readExp($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM exp WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch(PDO::FETCH_ASSOC);
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM exp WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        }else {
            $select = $this->mysql->prepare('SELECT * FROM exp WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        }

        $select->execute();
        return $select->fetch();
    }

    public function editExp($dados){
        $editar = $this->mysql->prepare('UPDATE exp SET pt = :pt, en = :en, es = :es WHERE id = :id ');
        $editar->bindValue(':pt', $dados['pt'], PDO::PARAM_STR);
        $editar->bindValue(':en', $dados['en'], PDO::PARAM_STR);
        $editar->bindValue(':es', $dados['es'], PDO::PARAM_STR);
        $editar->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $editar->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM exp WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
