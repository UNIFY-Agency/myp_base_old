<?php

$loader->get('src/Model/Lances');

class Ordens extends Lances{

  //legenda do status: 0 ou null = esperando, 1 = vencedor do anuncio, 2 = perdedor do anuncio
  
  public function pay($id, $pag_ref, $result_data){
  
    $select = $this->mysql->prepare('SELECT lances.id as lance, lances.anuncio, lances.espera, anuncios.pai FROM lances
                                    INNER JOIN anuncios ON lances.anuncio = anuncios.id
                                    WHERE lances.id = :id;');
    $select->bindValue(':id', $id , PDO::PARAM_INT);
    $select->execute();
    $resultado = $select->fetch(PDO::FETCH_ASSOC);
    //return $resultado;
    
    //setando status dos perdedores
    $update = $this->mysql->prepare('UPDATE lances
                                    INNER JOIN anuncios ON lances.anuncio = anuncios.id
                                    SET lances.status = 2 WHERE anuncios.pai = :id AND anuncios.id = :anuncio;');
    $update->bindValue(':id', $resultado['pai'] , PDO::PARAM_INT);
    $update->bindValue(':anuncio', $resultado['anuncio'], PDO::PARAM_INT);
    $update->execute();
    
    //atulizando anuncio
    $update = $this->mysql->prepare('UPDATE `anuncios` SET finalizado = 1 WHERE id = :id;');
    $update->bindValue(':id', $resultado['anuncio'] , PDO::PARAM_INT);
    $update->execute();

    //setando status do vencedor
    $update = $this->mysql->prepare('UPDATE lances SET status = 1, pagamento_id = :pagamento_id, pag_date = :pag_date, result_data = :result_data WHERE id = :id;');
    $update->bindValue(':pagamento_id', $pag_ref, PDO::PARAM_STR);
    $update->bindValue(':id', $id, PDO::PARAM_INT);
    $update->bindValue(':pag_date', date("Y-m-d H:i:s"), PDO::PARAM_STR);
    $update->bindValue(':result_data', $result_data, PDO::PARAM_STR);
    $update->execute(); 
  
  }

  public function getWait($id, $pagseguro_id){
    $select = $this->mysql->prepare('SELECT lances.id, lances.anuncio, lances.espera FROM lances
                                    INNER JOIN anuncios ON lances.anuncio = anuncios.id
                                    WHERE anuncios.pai = :id AND lances.espera != "0000-00-00 00:00:00";');
    $select->bindValue(':id', $id , PDO::PARAM_INT);
    $select->execute();
    $resultado = $select->fetchAll(PDO::FETCH_ASSOC);

    if(count($resultado) < 1){ return false; }

    $chave = array_search(max(array_column($resultado, 'espera')), array_column($resultado, 'espera'));

    //zerando todas as esperas desse usuário
    $update = $this->mysql->prepare('UPDATE lances
                                    INNER JOIN anuncios ON lances.anuncio = anuncios.id
                                    SET lances.espera = "0000-00-00 00:00:00" WHERE anuncios.pai = :id;');
    $update->bindValue(':id', $id , PDO::PARAM_INT);
    $update->execute();

    //setando status dos perdedores
    $update = $this->mysql->prepare('UPDATE lances
                                    INNER JOIN anuncios ON lances.anuncio = anuncios.id
                                    SET lances.status = 2 WHERE anuncios.pai = :id AND anuncios.id = :anuncio;');
    $update->bindValue(':id', $id , PDO::PARAM_INT);
    $update->bindValue(':anuncio', $resultado[$chave]['anuncio'], PDO::PARAM_INT);
    $update->execute();

    //setando status do vencedor
    $update = $this->mysql->prepare('UPDATE lances SET status = 1, pagamento_id = :pagamento_id WHERE id = :id;');
    $update->bindValue(':pagamento_id', $pagseguro_id, PDO::PARAM_STR);
    $update->bindValue(':id', $resultado[$chave]['id'], PDO::PARAM_INT);
    $update->execute();

  }

  //CRUD

  public function readOrders($id=null, $pai=null, $anuncio=null, $adtional_fields=null){
      if(!empty($id)) {
          $select = $this->mysql->prepare('SELECT * FROM lances INNER JOIN anuncios ON lances.anuncio = anuncios.id WHERE id = :id AND publicado = 1 AND status = 1;');
          $select->bindValue(':id', $id  , PDO::PARAM_INT);
          $select->execute();
          return $select->fetch(PDO::FETCH_ASSOC);
      } else if(!empty($pai)) {
          $select = $this->mysql->prepare('SELECT * FROM lances INNER JOIN anuncios ON lances.anuncio = anuncios.id WHERE pai = :id AND publicado = 1 AND status = 1;');
          $select->bindValue(':id', $pai  , PDO::PARAM_INT);
          $select->execute();
          return $select->fetchAll(PDO::FETCH_ASSOC);
      } else if(!empty($anuncio)) {
          $select = $this->mysql->prepare('SELECT * FROM lances INNER JOIN anuncios ON lances.anuncio = anuncios.id WHERE anuncio = :anuncio AND publicado = 1 AND status = 1;');
          $select->bindValue(':anuncio', $anuncio  , PDO::PARAM_INT);
          $select->execute();
          return $select->fetchAll(PDO::FETCH_ASSOC);
      } else {
          $select = $this->mysql->prepare('SELECT * '.$adtional_fields.' FROM lances INNER JOIN anuncios ON lances.anuncio = anuncios.id WHERE status IN(1,3) AND anuncios.pai = :id ORDER BY valor ASC;');
          $select->bindValue(':id', $_SESSION['usuario']['id']  , PDO::PARAM_INT);
          $select->execute();
          return $select->fetchAll(PDO::FETCH_ASSOC);
      }

    }
}
