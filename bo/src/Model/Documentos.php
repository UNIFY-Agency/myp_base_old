<?php

class Documentos{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public static function upFile($file, $allow=null){
      // Pasta onde o arquivo vai ser salvo
      $_UP['pasta'] = 'img/';
      // Tamanho máximo do arquivo (em Bytes)
      $_UP['tamanho'] = 1024 * 1024 * 4; // 4Mb
      // Renomeia o arquivo? (Se true, o arquivo será salvo com um hash md5)
      $_UP['renomeia'] = true;
      // Array com os tipos de erros de upload do PHP
      $_UP['erros'][0] = 'Não houve erro';
      $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
      $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
      $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
      $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
      // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
      if ($file[3] != 0) {
        return "Não foi possível fazer o upload, erro:" . $_UP['erros'][$file[3]];
      }
      // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
      // Faz a verificação da extensão do arquivo
      if(!is_null($allow)){
        $extensao = strtolower(end(explode('.', $file[0])));
        if (array_search($extensao, $allow) === false){
          return "Por favor, envie arquivos com as seguintes extensões: jpg, png, gif ou pdf";
        }
      }
      // Faz a verificação do tamanho do arquivo
      if ($_UP['tamanho'] < $file[4]) {
        return "O arquivo enviado é muito grande, envie arquivos de até 4Mb.";
      }
      // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
      // Primeiro verifica se deve trocar o nome do arquivo
      if ($_UP['renomeia'] == true) {
        // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
        $nome_final = md5(time()).'.'.$extensao;
      } else {
        // Mantém o nome original do arquivo
        $nome_final = $file[0];
      }

      // Depois verifica se é possível mover o arquivo para a pasta escolhida
      if (move_uploaded_file($file[2], $_UP['pasta'] . $nome_final)) {
        // Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
        return $nome_final;
      } else {
        // Não foi possível fazer o upload, provavelmente a pasta está incorreta
        return "Não foi possível enviar o arquivo, tente novamente";
      }
    }

    //CRUD

    public function insertDoc($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO documentos (pai, nome, endereco) VALUES (:pai, :nome, :endereco);');
            $cadastra->bindValue(':pai', $dados[5], PDO::PARAM_INT);
            $cadastra->bindValue(':nome', $dados[0], PDO::PARAM_STR);
            $cadastra->bindValue(':endereco', end($dados), PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readDoc($id=null, $name=null, $pai=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM documentos WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM documentos WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else if(!empty($pai)) {
            $select = $this->mysql->prepare('SELECT * FROM documentos WHERE pai = :pai');
            $select->bindValue(':pai', $pai  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetchAll();
        }else{
            $select = $this->mysql->prepare('SELECT * FROM documentos WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCat($dados){
        $deletef = $this->mysql->prepare('UPDATE documentos SET nome = :nome WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM documentos WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
