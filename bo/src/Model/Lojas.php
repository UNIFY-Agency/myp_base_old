<?php

class Lojas{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function listarLojasAtivas(){
      $select = $this->mysql->prepare('SELECT * FROM lojas WHERE ativo = 1 ORDER BY id ASC;');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function listarLojasMarca(){
      $select = $this->mysql->prepare('SELECT l.id, l.nome, m.nome as marca, l.pais, l.codigo, l.tel, l.morada, l.codigo_postal FROM lojas l
                                        INNER JOIN marcas m ON l.marca_id = m.id');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllLojasWithAutomaticAdvance(){
      $select = $this->mysql->prepare('SELECT * FROM `avanco_automatico_pedidos` WHERE ligado = 1');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getLojaAutomaticAdvance($lid){
      $select = $this->mysql->prepare('SELECT * FROM `avanco_automatico_pedidos` WHERE id_loja = :id');
      $select->bindValue(':id', $lid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function LojaAutomaticAdvanceOn($lid){
      $update = $this->mysql->prepare('UPDATE `avanco_automatico_pedidos` SET `ligado`= 1, `atualizado` = :atualizado WHERE id_loja = :id;');
      $update->bindValue(':id', $lid, PDO::PARAM_INT);
      $update->bindValue(':atualizado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $update->execute();
    }

    public function LojaAutomaticAdvanceOff($lid){
      $update = $this->mysql->prepare('UPDATE `avanco_automatico_pedidos` SET `ligado`= 0, `atualizado` = :atualizado WHERE id_loja = :id;');
      $update->bindValue(':id', $lid, PDO::PARAM_INT);
      $update->bindValue(':atualizado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $update->execute();
    }

    public function createLojaAutomaticAdvance($lid){
        $stmt = $this->mysql->prepare("SELECT * FROM `avanco_automatico_pedidos` WHERE loja_id = :id");
        $stmt->bindValue(':id', $lid, PDO::PARAM_INT);
        $stmt->execute();
        $loja = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($loja) {
            return 0;
        } else {
            $cadastra = $this->mysql->prepare('INSERT into avanco_automatico_pedidos (`id_loja`, `ligado`, `minutos`, `criado`) VALUES (:id_loja, 1, 20, :criado);');
            $cadastra->bindValue(':id_loja', $lid, PDO::PARAM_INT);
            $cadastra->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
            $cadastra->execute();
            return $this->mysql->lastInsertId();
        }

    }

    //CRUD

    public function insertCat($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO lojas (nome) VALUES (:nome);');
            $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readLojas($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM lojas WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM lojas WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM lojas WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCat($dados){
        $deletef = $this->mysql->prepare('UPDATE lojas SET nome = :nome WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM lojas WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
