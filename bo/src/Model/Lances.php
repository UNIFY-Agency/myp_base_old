<?php

class Lances{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    //legenda do status: 0 ou null = esperando, 1 = vencedor do anuncio, 2 = perdedor do anuncio, 3 = pendente de pagamento

    public function putWait($id){
      $select = $this->mysql->prepare('UPDATE lances SET espera = :espera WHERE id = :id;');
      $select->bindValue(':espera', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $select->bindValue(':id', $id, PDO::PARAM_INT);
      $select->execute();
    }
    
    public function removeWait($id){
  
      //zerando todas as esperas desse usu�rio
      $update = $this->mysql->prepare('UPDATE lances
                                      INNER JOIN anuncios ON lances.anuncio = anuncios.id
                                      SET lances.espera = "0000-00-00 00:00:00" WHERE anuncios.pai = :id;');
      $update->bindValue(':id', $id , PDO::PARAM_INT);
      $update->execute();
  
    }
    
    public function setStatus($id, $status, $ref, $result_data){
      $select = $this->mysql->prepare('UPDATE lances SET status = :status, pagamento_id = :pagamento_id, espera = "0000-00-00 00:00:00", result_data = :result_data WHERE id = :id;');
      $select->bindValue(':status', $status, PDO::PARAM_STR);
      $select->bindValue(':pagamento_id', $ref, PDO::PARAM_STR);
      $select->bindValue(':result_data', $result_data, PDO::PARAM_STR);
      $select->bindValue(':id', $id, PDO::PARAM_INT);
      $select->execute();
    }

    //CRUD

    public function cadastrarLance($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO lances (pai, anuncio, valor, inclusao, espera, espera) VALUES ('.$_SESSION['usuario']['id'].', :anuncio, :valor, :inclusao, 0);');
            $cadastra->bindValue(':anuncio', $dados['anuncio'], PDO::PARAM_STR);
            $cadastra->bindValue(':valor', $dados['valor'], PDO::PARAM_STR);
            $cadastra->bindValue(':inclusao', date('Y-m-d'), PDO::PARAM_STR);
            $cadastra->execute();
            //header('Location:index.php?pages=anuncio');
        }
    }

    public function makeLance($dados){

          $select = $this->mysql->prepare('SELECT id FROM lances WHERE `anuncio` = :anuncio AND pai = '.$_SESSION['usuario']['id'].';');
          $select->bindValue(':anuncio', $dados['anuncio'], PDO::PARAM_STR);
          $select->execute();
          $count = $select->fetchAll();

          if(count($count) > 0){ return false; } else {

            $cadastra = $this->mysql->prepare('INSERT INTO lances (pai, anuncio, valor, inclusao, publicado, espera) VALUES ('.$_SESSION['usuario']['id'].', :anuncio, :valor, :inclusao, 1, 0);');
            $cadastra->bindValue(':anuncio', $dados['anuncio'], PDO::PARAM_STR);
            $cadastra->bindValue(':valor', $dados['valor'], PDO::PARAM_STR);
            $cadastra->bindValue(':inclusao', date('Y-m-d'), PDO::PARAM_STR);
            $cadastra->execute();

            return true;
          }
    }

    public function readLances($nome=null, $id=null, $pai=null, $anuncio=null){
        if(!empty($nome)) {
            $select = $this->mysql->prepare('SELECT * FROM lances WHERE titulo = :nome AND publicado = 1;');
            $select->bindValue(':nome', $nome, PDO::PARAM_STR);
            $select->execute();
            return $select->fetch();
        } else if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM lances WHERE id = :id AND publicado = 1;');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($pai)) {
            $select = $this->mysql->prepare('SELECT * FROM lances WHERE pai = :id AND publicado = 1;');
            $select->bindValue(':id', $pai  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetchAll();
        } else if(!empty($anuncio)) {
            $select = $this->mysql->prepare('SELECT * FROM lances WHERE anuncio = :anuncio AND publicado = 1;');
            $select->bindValue(':anuncio', $anuncio  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetchAll();
        } else {
            $select = $this->mysql->prepare('SELECT * FROM lances WHERE 1 ORDER BY valor ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function readPubAnuncio($nome=null, $id=null){
        if(!empty($nome)) {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE titulo = :nome AND ativ = 1 AND publicado = 1;');
            $select->bindValue(':nome', $nome, PDO::PARAM_STR);
            $select->execute();
            return $select->fetch();
        } else if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE id = :id AND ativ = 1 AND publicado = 1;');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else {
            $select = $this->mysql->prepare('SELECT * FROM anuncios WHERE ativ = 1 AND publicado = 1;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function deleteAnuncio($id){
        $delete = $this->mysql->prepare('UPDATE anuncios SET ativ = 0 WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        $delete->execute();

        $foto = $this->searchFoto($id);
        foreach($foto as $fotos){ unlink($fotos['local']); }

        $deletef = $this->mysql->prepare('DELETE FROM fotos WHERE id_pai = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
