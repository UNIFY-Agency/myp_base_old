jQuery( document ).ready(function() {
    //idioma
    jQuery('.lang').click(function(){
        // console.log(jQuery(this).attr('lang'));
        var url = URL_BASE + '/language/change/' + jQuery(this).attr('lang');
        jQuery.post( url, function(){ location.reload(); });
    });

    jQuery('.cookie-ok').click(function(){
        //console.log(jQuery(this).attr('class'));
        var url = URL_BASE + '/cookies/allow/';
        jQuery.post( url, function(){ jQuery('.notice-bottom-bar').hide(); });
    });

    jQuery('.header-search-mobile').click(function(){
      console.log('clique');
      jQuery('.search-line').toggle("slow");
    });

    $('.dropdown-mega-sub-title').click( function(){

      // console.log($(this).attr('class'));

      if(!$(this).hasClass('active-menu')){
        $(".dropdown-mega-sub-title").removeClass("active-menu").find('.arrowdown-active').removeClass().addClass('arrowdown').attr('src', URL_BASE + '/assets/img/icons/arrowdown.svg');
        $(this).addClass('active-menu').find('.arrowdown').removeClass().addClass('arrowdown-active').attr('src', URL_BASE + '/assets/img/icons/arrowdown-active.svg');
        // console.log('n tem');
      }

    });

    //destaque no menu
    // if( window.location.pathname == '/' ) {
    //     var page = '/inicio';
    // } else {
    //     var page = window.location.pathname;
    // }
    // var page_class = '.'+page.substr(1);
    // $( page_class ).last().addClass( "current" );

    $(window).on("scroll", function() {
      // console.log($(window).scrollTop());
      if ($(window).scrollTop() > 100) {
        $('.side-header-narrow-bar').css("background-color", "#101820");
      } else {
        $('.side-header-narrow-bar').css('background-color', 'transparent');
      }
    });

});
