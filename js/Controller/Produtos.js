$('.eurotecnologia-filter').click( function(){
  var area = $(this).attr('area');
  var marca = $(this).attr('marca');
  var categoria = $(this).html();

  console.log($(this).attr('class'));

  $('.eurotecnologia-brand').html(marca);
  $('.eurotecnologia-category').html(categoria);

});

$('.eurotecnologia-brands').click( function(){
  var marca = $(this).html().replace(/(<([^>]+)>)/gi, "");
  var categoria = '';

  console.log($(this).attr('class'));

  $('.eurotecnologia-brand').html(marca);
  $('.eurotecnologia-category').html(categoria);

});

$('.filter-url').click( function(){
  window.location = $(this).attr('filter');
});

window.onload = function() {
  killLoader();
};

function killLoader(){
  $('.eurotecnologia-loader').fadeOut();
}

$('.cat-modal').click( function(){

  var img_url = $(this).attr('img_url');
  var catalogo_id = $(this).attr('mic');

  // console.log(img_url);

  $('.catalogo-logo').attr('src', img_url);
  $('#cid').val(catalogo_id);

});

$("#send-catalog").on('submit',(function(e) {
    e.preventDefault();

    var btn = $('#send-catalog-submit');
    btn.prop('disabled', true);
    $(btn).html($(btn).attr("data-loading-text"));
    setTimeout(function(){
        btn.prop('disabled', false);
        $(btn).html('<i class="far fa-envelope mr-2"></i>Enviar');
    }, 3*2000);

    var dados = $("#send-catalog").serializeArray();
    dados.push({name: "user_session_id", value: USER_SID});

    // console.log(dados);

    $.post( URL_BASE + '/Catalogos/send', {dados}, function( result ) {
        console.log(result);
        var results = jQuery.parseJSON(result);
        if(results[0] == 0) {
          $("#notifications-cat").removeClass().addClass("alert alert-danger").html(results[1]).fadeIn().delay(4000).fadeOut();
          document.getElementById("send-catalog").reset();
        } else {
          $("#notifications-cat").removeClass().addClass("alert alert-success").html(results[1]).fadeIn().delay(2000).fadeOut();
          document.getElementById("send-product").reset();
        }
    });

}));

$('.prod-modal').click( function(){

  var pid = $(this).attr('pid');

  // console.log(pid);

  $('#pid').val(pid);

});

$("#send-product").on('submit',(function(e) {
    e.preventDefault();

    var btn = $('#send-product-submit');
    btn.prop('disabled', true);
    $(btn).html($(btn).attr("data-loading-text"));
    setTimeout(function(){
        btn.prop('disabled', false);
        $(btn).html('<i class="far fa-envelope mr-2"></i>Enviar');
    }, 3*2000);

    var dados = $("#send-product").serializeArray();
    dados.push({name: "user_session_id", value: USER_SID});

    console.log(dados);

    $.post( URL_BASE + '/Produtos/send', {dados}, function( result ) {
        console.log(result);
        var results = jQuery.parseJSON(result);
        if(results[0] == 0) {
          $("#notifications-prod").removeClass().addClass("alert alert-danger").html(results[1]).fadeIn().delay(4000).fadeOut();
          document.getElementById("send-product").reset();
        } else {
          $("#notifications-prod").removeClass().addClass("alert alert-success").html(results[1]).fadeIn().delay(2000).fadeOut();
          document.getElementById("send-product").reset();
        }
    });

}));
